package com.menko.model;

public class PushUp implements TrainStuff{

    private int setCount;
    private int doExerciseCount;

    @Override
    public String doExercise() {
        return this.toString() +  " count for all sets " + (setCount * doExerciseCount) + " times";
    }

    public PushUp(int setCount, int doExerciseCount) {
        this.setCount = setCount;
        this.doExerciseCount = doExerciseCount;
    }

    public int getSetCount() {
        return setCount;
    }

    public void setSetCount(int setCount) {
        this.setCount = setCount;
    }

    public int getDoExerciseCount() {
        return doExerciseCount;
    }

    public void setDoExerciseCount(int doExerciseCount) {
        this.doExerciseCount = doExerciseCount;
    }

    @Override
    public String toString() {
        return "PushUp{" +
                "setCount=" + setCount +
                ", doExerciseCount=" + doExerciseCount +
                '}';
    }
}
