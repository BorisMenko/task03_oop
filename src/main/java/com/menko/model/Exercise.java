package com.menko.model;

import java.util.ArrayList;
import java.util.List;

public class Exercise {
    private List<TrainStuff> exercise;

    public Exercise() {
        exercise = new ArrayList<>();
        exercise.add(new RaceTrack(10, 3));
        exercise.add(new BenchPress(3, 12, 70));
        exercise.add(new SitWithGriff(3, 8, 100));
        exercise.add(new PushUp(5, 12));
    }

    public List<TrainStuff> getAllExercise() {
        return exercise;
    }


}
