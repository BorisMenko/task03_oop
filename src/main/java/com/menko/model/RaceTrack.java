package com.menko.model;

public class RaceTrack implements TrainStuff {

   private int timeOfSetExercise;
   private int setOfExercise;

    @Override
    public String doExercise() {
        return this.toString() + " all time exercise " + (timeOfSetExercise * setOfExercise) + " minutes";
    }

    public RaceTrack(int timeOfSetExercise, int setOfExercise) {
        this.timeOfSetExercise = timeOfSetExercise;
        this.setOfExercise = setOfExercise;
    }

    public int getTimeOfSetExercise() {
        return timeOfSetExercise;
    }

    public void setTimeOfSetExercise(int timeOfSetExercise) {
        this.timeOfSetExercise = timeOfSetExercise;
    }

    public int getSetOfExercise() {
        return setOfExercise;
    }

    public void setSetOfExercise(int setOfExercise) {
        this.setOfExercise = setOfExercise;
    }

    @Override
    public String toString() {
        return "RaceTrack{" +
                "timeOfSetExercise=" + timeOfSetExercise +
                ", setOfExercise=" + setOfExercise +
                '}';
    }
}
