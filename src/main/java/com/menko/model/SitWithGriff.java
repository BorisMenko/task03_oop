package com.menko.model;

public class SitWithGriff implements TrainStuff {

    private int setCount;
    private int doExerciseCount;
    private double weight;

    @Override
    public String doExercise() {
        return this.toString() +  " count for all sets " + (setCount * doExerciseCount)
                + " times " + "with " + weight + " kilograms";
    }

    public SitWithGriff(int setCount, int doExerciseCount, double weight) {
        this.setCount = setCount;
        this.doExerciseCount = doExerciseCount;
        this.weight = weight;
    }

    public int getSetCount() {
        return setCount;
    }

    public void setSetCount(int setCount) {
        this.setCount = setCount;
    }

    public int getDoExerciseCount() {
        return doExerciseCount;
    }

    public void setDoExerciseCount(int doExerciseCount) {
        this.doExerciseCount = doExerciseCount;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "SitWithGriff{" +
                "setCount=" + setCount +
                ", doExerciseCount=" + doExerciseCount +
                ", weight=" + weight +
                '}';
    }
}
