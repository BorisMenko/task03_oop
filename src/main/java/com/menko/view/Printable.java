package com.menko.view;

@FunctionalInterface
public interface Printable {

    void print();
}
