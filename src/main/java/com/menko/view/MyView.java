package com.menko.view;

import com.menko.controller.Controller;
import com.menko.controller.ControllerImp;
import com.menko.model.TrainStuff;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public void printList(List<TrainStuff> trainStuffs) {
        for (TrainStuff s : trainStuffs) {
            System.out.println(s.doExercise());
        }
    }

    public MyView() {
        controller = new ControllerImp();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - get all exercise");
        menu.put("2", "  2 - get exercise for weight lost");
        menu.put("3", "  3 - get exercise for weight gain");
        menu.put("4", "  4 - get exercise for stamina gain");
        menu.put("5", "  5 - get exercise for power gain");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
    }

    private void pressButton1() {
        printList(controller.getAllExercise());
    }

    private void pressButton2() {
        printList(controller.getExerciseForWeightLost());
    }

    private void pressButton3() {
        printList(controller.getExerciseForWeightGain());
    }

    private void pressButton4() {
        printList(controller.getExerciseForStaminaGain());
    }

    private void pressButton5() {
        printList(controller.getExerciseForPowerGain());
    }


    private void outputMenu() {
        System.out.println("\nTraining list");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select training point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
