package com.menko.controller;

import com.menko.model.TrainStuff;

import java.util.List;

public interface Controller {

    List<TrainStuff> getAllExercise();
    List<TrainStuff> getExerciseForWeightLost();
    List<TrainStuff> getExerciseForWeightGain();
    List<TrainStuff> getExerciseForStaminaGain();
    List<TrainStuff> getExerciseForPowerGain();
}
