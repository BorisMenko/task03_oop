package com.menko.controller;

import com.menko.model.*;

import java.util.ArrayList;
import java.util.List;

public class ControllerImp implements Controller {
    private Exercise exercise;

    public ControllerImp() {
        exercise = new Exercise();
    }

    @Override
    public List<TrainStuff> getAllExercise() {
        return exercise.getAllExercise();
    }

    @Override
    public List<TrainStuff> getExerciseForWeightLost() {
        List<TrainStuff> exerciseForWeightLost = new ArrayList<>();
        for (TrainStuff s: exercise.getAllExercise()) {
            if (s instanceof RaceTrack || s instanceof SitWithGriff){
                exerciseForWeightLost.add(s);
            }
        }
        return exerciseForWeightLost;
    }

    @Override
    public List<TrainStuff> getExerciseForWeightGain() {
        List<TrainStuff> exerciseForWeightGain = new ArrayList<>();
        for (TrainStuff s: exercise.getAllExercise()){
            if (s instanceof BenchPress || s instanceof SitWithGriff){
                exerciseForWeightGain.add(s);
            }
        }
        return exerciseForWeightGain;
    }

    @Override
    public List<TrainStuff> getExerciseForStaminaGain() {
        List<TrainStuff> exerciseForStaminaGain = new ArrayList<>();
        for (TrainStuff s: exercise.getAllExercise()) {
            if (s instanceof RaceTrack || s instanceof PushUp){
                exerciseForStaminaGain.add(s);
            }
        }
        return exerciseForStaminaGain;
    }

    @Override
    public List<TrainStuff> getExerciseForPowerGain() {
        List<TrainStuff> exerciseForPowerGain = new ArrayList<>();
        for (TrainStuff s: exercise.getAllExercise()) {
            if (s instanceof PushUp || s instanceof BenchPress){
                exerciseForPowerGain.add(s);
            }
        }
        return exerciseForPowerGain;
    }
}
